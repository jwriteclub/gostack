/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gostack/models/site"
	"fmt"
	"github.com/tenta-browser/polychromatic"
)

var (
	pathSites                = "delivery/v1/stacks/%s/sites"
	pathSitesById            = "delivery/v1/stacks/%s/sites/%s"
	pathSitesDeliveryDomains = "delivery/v1/stacks/%s/sites/%s/delivery_domains"
	pathStackOrigins         = "delivery/v1/stacks/%s/origins"
)

func (c *Client) DeleteSite(stackId, siteId string) error {
	r, err := c.deleteRequest(fmt.Sprintf(pathSitesById, stackId, siteId))
	if err != nil {
		return fmt.Errorf("unable to delete site request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return fmt.Errorf("unable to delete site auth: %w", err)
	}
	var resp map[string]interface{}
	err = c.doWithArgs(r, map[string]string{}, &resp)
	if err != nil {
		return fmt.Errorf("could not delete site: %w", err)
	}
	polychromatic.GetLogger("api-delete-site").WithField("site_id", siteId).Debugf("Deleted site: %v", resp)
	return nil
}

func (c *Client) ListSites(stackId string) ([]site.Site, error) {
	r, err := c.getRequest(fmt.Sprintf(pathSites, stackId))
	if err != nil {
		return nil, fmt.Errorf("unable to list sites request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list sites auth: %w", err)
	}
	sites := make([]site.Site, 0)
	after := "0"
	for {
		var s listSitesAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
		}, &s)
		if err != nil {
			return nil, fmt.Errorf("could not list sites: %w", err)
		}
		sites = append(sites, s.Stacks...)
		if s.PI.HasNextPage {
			after = s.PI.EndCursor
		} else {
			break
		}
	}
	return sites, nil
}

func (c *Client) ListDeliveryDomains(stackId, siteId string) ([]site.DeliveryDomain, error) {
	r, err := c.getRequest(fmt.Sprintf(pathSitesDeliveryDomains, stackId, siteId))
	if err != nil {
		return nil, fmt.Errorf("unable to list delivery domains request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list delivery domains auth: %w", err)
	}
	domains := make([]site.DeliveryDomain, 0)
	after := "0"
	for {
		var s listSitesDeliveryDomainsAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
		}, &s)
		if err != nil {
			return nil, fmt.Errorf("could not delivery domains sites: %w", err)
		}
		domains = append(domains, s.Domains...)
		if s.PI.HasNextPage {
			after = s.PI.EndCursor
		} else {
			break
		}
	}
	return domains, nil
}

func (c *Client) ListOrigins(stackId, siteId string) ([]site.Origin, error) {
	r, err := c.getRequest(fmt.Sprintf(pathStackOrigins, stackId))
	if err != nil {
		return nil, fmt.Errorf("unable to list origins request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list origins auth: %w", err)
	}
	origins := make([]site.Origin, 0)
	after := "0"
	for {
		var s listSiteOriginsAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
		}, &s)
		if err != nil {
			return nil, fmt.Errorf("could not list origins: %w", err)
		}
		for idx := range s.Origins {
			if s.Origins[idx].SiteId == siteId {
				origins = append(origins, s.Origins[idx])
			}
		}
		if s.PI.HasNextPage {
			after = s.PI.EndCursor
		} else {
			break
		}
	}
	return origins, nil
}
