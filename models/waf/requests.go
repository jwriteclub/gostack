/*
 * GoStack API Client
 *
 *  Copyright 2021 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package waf

import "strconv"

type CountableStatistic interface {
	GetCount() (int64, error)
	MustGetCount() int
}

type RequestStatisticsRule struct {
	RuleName string `json:"ruleName"`
	Count    string `json:"count"`
}

func (r RequestStatisticsRule) GetCount() (int64, error) {
	return strconv.ParseInt(r.Count, 10, 64)
}
func (r RequestStatisticsRule) MustGetCount() int {
	ret, err := r.GetCount()
	if err != nil {
		panic(err.Error())
	}
	return int(ret)
}

type RequestStatisticsCountry struct {
	Country     string `json:"country"`
	CountryCode string `json:"countryCode"`
	Count       string `json:"count"`
}

func (r RequestStatisticsCountry) GetCount() (int64, error) {
	return strconv.ParseInt(r.Count, 10, 64)
}
func (r RequestStatisticsCountry) MustGetCount() int {
	ret, err := r.GetCount()
	if err != nil {
		panic(err.Error())
	}
	return int(ret)
}

type RequestStatisticsOrganization struct {
	Organization string `json:"organization"`
	Count        string `json:"count"`
}

func (r RequestStatisticsOrganization) GetCount() (int64, error) {
	return strconv.ParseInt(r.Count, 10, 64)
}
func (r RequestStatisticsOrganization) MustGetCount() int {
	ret, err := r.GetCount()
	if err != nil {
		panic(err.Error())
	}
	return int(ret)
}

type RequestStatisticsAction struct {
	Action string `json:"action"`
	Count  string `json:"count"`
}

func (r RequestStatisticsAction) GetCount() (int64, error) {
	return strconv.ParseInt(r.Count, 10, 64)
}
func (r RequestStatisticsAction) MustGetCount() int {
	ret, err := r.GetCount()
	if err != nil {
		panic(err.Error())
	}
	return int(ret)
}

type RequestStatisticsData struct {
	Rules         []RequestStatisticsRule         `json:"rules"`
	Countries     []RequestStatisticsCountry      `json:"countries"`
	Organizations []RequestStatisticsOrganization `json:"organizations"`
	Actions       []RequestStatisticsAction       `json:"actions"`
}
type RequestStatistics struct {
	Total   RequestStatisticsData `json:"count"`
	Blocked RequestStatisticsData `json:"blocked"`
}

type RequestDetails struct {
	Id              string `json:"id"`
	Path            string `json:"path"`
	ClientIPString  string `json:"clientIp"`
	Method          string `json:"method"`
	StatusCode      string `json:"statusCode"`
	RuleName        string `json:"ruleName"`
	CountryCode     string `json:"country"`
	Action          string `json:"action"`
	Result          string `json:"result"`
	RuleID          string `json:"ruleId"`
	UserAgent       string `json:"userAgent"`
	UserAgentClient string `json:"userAgentClient"`
	Organization    string `json:"organization"`
	RequestTime     string `json:"requestTime"`
	ReferenceID     string `json:"referenceId"`
}
