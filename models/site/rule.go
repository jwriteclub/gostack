/*
 * GoStack API Client
 *
 *  Copyright 2022 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package site

type Action string

const (
	ActionBlock     Action = "BLOCK"
	ActionAllow            = "ALLOW"
	ActionCaptcha          = "CAPTCHA"
	ActionHandshake        = "HANDSHAKE"
	ActionMonitor          = "MONITOR"
	ActionGateway          = "GATEWAY"
	ActionUnknown          = "UNKNOWN"
)

type Rule struct {
	Id             string      `json:"id"`
	Name           string      `json:"name"`
	Description    string      `json:"description"`
	Conditions     []Condition `json:"conditions"`
	Action         string      `json:"action"`
	Enabled        bool        `json:"enabled"`
	ActionDuration string      `json:"actionDuration"`
	StatusCode     string      `json:"statusCode"`
}

func (r Rule) GetAction() Action {
	for _, a := range []Action{ActionBlock, ActionAllow, ActionCaptcha, ActionHandshake, ActionMonitor, ActionGateway} {
		if r.Action == string(a) {
			return a
		}
	}
	return ActionUnknown
}

func (r Rule) IsIPOnlyRule() bool {
	ret := true
	for _, c := range r.Conditions {
		ret = ret && c.IsIPOnly()
	}
	return ret
}
