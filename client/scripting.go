/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gostack/models/scripting"
	"fmt"
)

var (
	pathScripts     = "cdn/v1/stacks/%s/sites/%s/scripts"
	pathScriptsById = "cdn/v1/stacks/%s/sites/%s/scripts/%s"
)

func (c *Client) ListScripts(stackId, siteId string) ([]scripting.Script, error) {
	r, err := c.getRequest(fmt.Sprintf(pathScripts, stackId, siteId))
	if err != nil {
		return nil, fmt.Errorf("unable to list scripts request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list scripts auth: %w", err)
	}
	scripts := make([]scripting.Script, 0)
	after := "0"
	for {
		var s listScriptsAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
		}, &s)
		if err != nil {
			return nil, fmt.Errorf("could not list scripts: %w", err)
		}
		scripts = append(scripts, s.Scripts...)
		if s.PI.HasNextPage {
			after = s.PI.EndCursor
		} else {
			break
		}
	}
	return scripts, nil
}

func (c *Client) LoadScript(stackId, siteId, scriptId string) (*scripting.Script, error) {
	r, err := c.getRequest(fmt.Sprintf(pathScriptsById, stackId, siteId, scriptId))
	if err != nil {
		return nil, fmt.Errorf("unable to load script request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to load script auth: %w", err)
	}
	var s scriptAPIResponse
	err = c.doWithArgs(r, map[string]string{}, &s)
	if err != nil {
		return nil, fmt.Errorf("could not load script: %w", err)
	}
	return &s.Script, nil
}

func (c *Client) LoadScriptVersion(stackId, siteId, scriptId, version string) (*scripting.Script, error) {
	r, err := c.getRequest(fmt.Sprintf(pathScriptsById, stackId, siteId, scriptId))
	if err != nil {
		return nil, fmt.Errorf("unable to list script version request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list script version auth: %w", err)
	}
	var s scriptAPIResponse
	err = c.doWithArgs(r, map[string]string{
		"script_version": version,
	}, &s)
	if err != nil {
		return nil, fmt.Errorf("could not load script version: %w", err)
	}
	return &s.Script, nil
}

func (c *Client) CreateScript(sc *scripting.Script) (*scripting.Script, error) {
	r, err := c.postRequest(fmt.Sprintf(pathScripts, sc.StackId, sc.SiteId))
	if err != nil {
		return nil, fmt.Errorf("unable to post script request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to post script auth: %w", err)
	}
	var s scriptAPIResponse
	err = c.doWithBody(r, map[string]interface{}{
		"name":  sc.Name,
		"code":  sc.Code,
		"paths": sc.Paths,
	}, &s)
	if err != nil {
		return nil, fmt.Errorf("could not post script: %w", err)
	}
	return &s.Script, nil
}

func (c *Client) UpdateScript(sc *scripting.Script) (*scripting.Script, error) {
	r, err := c.patchRequest(fmt.Sprintf(pathScriptsById, sc.StackId, sc.SiteId, sc.Id))
	if err != nil {
		return nil, fmt.Errorf("unable to update script %s request: %w", sc.Name, err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to update script %s auth: %w", sc.Name, err)
	}
	var s scriptAPIResponse
	err = c.doWithBody(r, map[string]interface{}{
		"code":  sc.Code,
		"paths": sc.Paths,
	}, &s)
	if err != nil {
		return nil, fmt.Errorf("could not update script %s: %w", sc.Name, err)
	}
	return &s.Script, nil
}

func (c *Client) DeleteScript(sc *scripting.Script) error {
	r, err := c.deleteRequest(fmt.Sprintf(pathScriptsById, sc.StackId, sc.SiteId, sc.Id))
	if err != nil {
		return fmt.Errorf("unable to delete script %s request: %w", sc.Name, err)
	}
	err = c.addAuth(r)
	if err != nil {
		return fmt.Errorf("unable to delete script %s auth: %w", sc.Name, err)
	}
	err = c.doBareRequest(r)
	if err != nil {
		return fmt.Errorf("could not delete script %s: %w", sc.Name, err)
	}
	return nil
}
