/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package config

import (
	"fmt"

	"github.com/spf13/viper"
)

const ClientIdName = "stack-client-id"
const ClientSecretName = "stack-client-secret"
const AccountIdName = "stack-account-id"
const StackName = "stack-id"
const DefaultEndpoint = "https://gateway.stackpath.com/"

var (
	ErrNoClientId     = fmt.Errorf("no %s specified", ClientIdName)
	ErrNoClientSecret = fmt.Errorf("no %s specified", ClientSecretName)
	ErrNoAccountId    = fmt.Errorf("no %s specified", AccountIdName)
	ErrNoStack        = fmt.Errorf("no %s specified", StackName)
)

type Credentials struct {
	ClientId     string
	ClientSecret string
	AccountId    string
	Stack        string
}

func GetCredentials() (*Credentials, error) {
	ret := &Credentials{}

	id := viper.GetString(ClientIdName)
	if id == "" {
		return nil, ErrNoClientId
	}
	ret.ClientId = id

	secret := viper.GetString(ClientSecretName)
	if secret == "" {
		return nil, ErrNoClientSecret
	}
	ret.ClientSecret = secret

	acct := viper.GetString(AccountIdName)
	if acct == "" {
		return nil, ErrNoAccountId
	}
	ret.AccountId = acct

	stack := viper.GetString(StackName)
	if stack == "" {
		return nil, ErrNoStack
	}
	ret.Stack = stack

	return ret, nil
}
