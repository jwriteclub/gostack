/*
 * GoStack API Client
 *
 *  Copyright 2022 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gostack/models/waf"
	"fmt"
)

const (
	pathStackWafDdos = "waf/v1/stacks/%s/sites/%s/ddos"
)

func (c *Client) ListWafDDoS(stackId, siteId string) (waf.DDoS, error) {
	r, err := c.getRequest(fmt.Sprintf(pathStackWafDdos, stackId, siteId))
	if err != nil {
		return waf.DDoS{}, fmt.Errorf("unable to list site ddos request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return waf.DDoS{}, fmt.Errorf("unable to list site ddos auth: %w", err)
	}
	var s listWafDDoSAPIResponse
	err = c.doWithArgs(r, map[string]string{}, &s)
	if err != nil {
		return waf.DDoS{}, fmt.Errorf("could not list site ddos: %w", err)
	}
	return s.DDoSSettings, nil
}

func (c *Client) UpdateWafDDoS(stackId, siteId string, global, burst, subsecond int) (waf.DDoS, error) {
	r, err := c.patchRequest(fmt.Sprintf(pathStackWafDdos, stackId, siteId))
	if err != nil {
		return waf.DDoS{}, fmt.Errorf("unable to patch site ddos request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return waf.DDoS{}, fmt.Errorf("unable to patch site ddos auth: %w", err)
	}
	var s listWafDDoSAPIResponse
	err = c.doWithBody(r, map[string]interface{}{
		"globalThreshold":         fmt.Sprintf("%d", global),
		"burstThreshold":          fmt.Sprintf("%d", burst),
		"subSecondBurstThreshold": fmt.Sprintf("%d", subsecond),
	}, &s)
	if err != nil {
		return waf.DDoS{}, fmt.Errorf("could not patch site ddos: %w", err)
	}
	return s.DDoSSettings, nil
}
