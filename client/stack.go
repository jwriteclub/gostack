/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gostack/models/stack"
	"fmt"
	"github.com/tenta-browser/polychromatic"
)

var (
	pathStacks     = "stack/v1/stacks"
	pathStacksById = "stack/v1/stacks/%s"
)

const DeleteNow = "2006-01-02T15:04:05.999999999Z"

func (c *Client) DeleteStack(stackId string, when string) error {
	r, err := c.deleteRequest(fmt.Sprintf(pathStacksById, stackId))
	if err != nil {
		return fmt.Errorf("unable to delete stack request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return fmt.Errorf("unable to delete stack auth: %w", err)
	}
	if when == "" {
		return fmt.Errorf("delete stack requires a when")
	}
	var resp map[string]interface{}
	err = c.doWithArgs(r, map[string]string{
		"not_before": when,
	}, &resp)
	if err != nil {
		return fmt.Errorf("could not delete stack: %w", err)
	}
	polychromatic.GetLogger("api-delete-stack").WithField("stack_id", stackId).Debugf("Deleted stack: %v", resp)
	return nil
}

func (c *Client) ListStacks() ([]stack.Stack, error) {
	r, err := c.getRequest(pathStacks)
	if err != nil {
		return nil, fmt.Errorf("unable to list stacks request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list stacks auth: %w", err)
	}
	stacks := make([]stack.Stack, 0)
	after := "0"
	for {
		var s listStacksAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
			"account_id":         c.creds.AccountId,
		}, &s)
		if err != nil {
			return nil, fmt.Errorf("could not list scripts: %w", err)
		}
		stacks = append(stacks, s.Stacks...)
		if s.PI.HasNextPage {
			after = s.PI.EndCursor
		} else {
			break
		}
	}
	return stacks, nil
}

func (c *Client) LoadStack(stackId string) (*stack.Stack, error) {
	r, err := c.getRequest(fmt.Sprintf(pathStacksById, stackId))
	if err != nil {
		return nil, fmt.Errorf("unable to load stack request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to load stack auth: %w", err)
	}
	var s stack.Stack
	err = c.doWithArgs(r, map[string]string{}, &s)
	if err != nil {
		return nil, fmt.Errorf("could not load stack: %w", err)
	}
	return &s, nil
}
