/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gostack/models/dns"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/wpalmer/gozone"
	"strings"
	"time"
)

var (
	pathDnsListZones     = "dns/v1/stacks/%s/zones"
	pathDnsUpsertRecords = "dns/v1/stacks/%s/zones/%s/bulk/records"
	pathDnsDeleteRecords = "dns/v1/stacks/%s/zones/%s/bulk/records/delete"
	pathDnsListRecords   = "dns/v1/stacks/%s/zones/%s/records"
	pathDnsZones         = "dns/v1/stacks/%s/zones"
	pathDnsZonesById     = "dns/v1/stacks/%s/zones/%s"
)

func (c *Client) ListDomains(stack_id string) ([]dns.Zone, error) {
	r, err := c.getRequest(fmt.Sprintf(pathDnsListZones, stack_id))
	if err != nil {
		return nil, fmt.Errorf("unable to list domains request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list domains auth: %w", err)
	}
	zones := make([]dns.Zone, 0)
	after := "0"
	for {
		var z zoneAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
		}, &z)
		if err != nil {
			return nil, fmt.Errorf("could not list domains: %w", err)
		}
		zones = append(zones, z.Zones...)
		if z.PI.HasNextPage {
			after = z.PI.EndCursor
		} else {
			break
		}
	}
	return zones, nil
}

func (c *Client) LoadDomain(stack_id, name string) (*dns.Zone, []dns.ExtendedRecord, error) {
	name = dns.NormalizeName(name)
	names, err := c.ListDomains(stack_id)
	if err != nil {
		return nil, nil, err
	}
	var z dns.Zone
	found := false
	for _, n := range names {
		if name == n.Domain {
			z = n
			found = true
			break
		}
	}
	if !found {
		return nil, nil, fmt.Errorf("could not find %s: %w", name, ErrDomainNotFound)
	}
	r, err := c.getRequest(fmt.Sprintf(pathDnsListRecords, c.creds.Stack, z.ID))
	if err != nil {
		return nil, nil, fmt.Errorf("unable to list records request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to list records auth: %w", err)
	}
	records := make([]dns.ResourceRecord, 0)
	after := "0"
	for {
		var e recordsAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
		}, &e)
		if err != nil {
			return nil, nil, fmt.Errorf("could not list domains: %w", err)
		}
		records = append(records, e.Records...)
		if e.PI.HasNextPage {
			after = e.PI.EndCursor
		} else {
			break
		}
	}
	ret, err := parseZoneLabels(&z)
	if err != nil {
		return nil, nil, fmt.Errorf("could not list domain labels: %w", err)
	}
	for _, rec := range records {
		ret = append(ret, rec.ExtendedRecord(z.Domain))
	}
	ret = dns.SortRecords(ret)
	return &z, ret, nil
}

func (c *Client) CreateZone(name string) (*dns.Zone, error) {
	name = dns.NormalizeName(name)
	r, err := c.postRequest(fmt.Sprintf(pathDnsZones, c.creds.Stack))
	if err != nil {
		return nil, fmt.Errorf("unable to create zone %s: %w", name, err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to create zone auth: %w", err)
	}
	zcr := zoneCreateResponse{}
	err = c.doWithBody(r, map[string]interface{}{
		"stackid":       c.creds.Stack,
		"domain":        name,
		"useApexDomain": false,
	}, &zcr)
	if err != nil {
		return nil, fmt.Errorf("could not create zone %s: %w", name, err)
	}
	return &zcr.Z, nil
}

func (c *Client) DeleteZone(z *dns.Zone) error {
	r, err := c.deleteRequest(fmt.Sprintf(pathDnsZonesById, z.StackID, z.ID))
	if err != nil {
		return fmt.Errorf("unable to delete zone %s: %w", z.Domain, err)
	}
	err = c.addAuth(r)
	if err != nil {
		return fmt.Errorf("unable to delete zone auth: %w", err)
	}
	err = c.doBareRequest(r)
	if err != nil {
		return fmt.Errorf("could not delete zone %s: %w", z.Domain, err)
	}
	return nil
}

func (c *Client) UpdateRecords(z *dns.Zone, changeset []dns.ChangeRecord) (int, error) {
	origin := dns.NormalizeName(z.Domain)
	updateLabels, labels, err := buildZoneLabels(z, changeset)
	if err != nil {
		return 0, fmt.Errorf("unable to build zone update labels: %w", err)
	}
	if updateLabels {
		r, err := c.putRequest(fmt.Sprintf(pathDnsZonesById, c.creds.Stack, z.ID))
		if err != nil {
			return 0, fmt.Errorf("unable to update zone %s: %w", origin, err)
		}
		err = c.addAuth(r)
		if err != nil {
			return 0, fmt.Errorf("unable to update zone auth: %w", err)
		}
		var rz dns.Zone
		err = c.doWithBody(r, map[string]interface{}{
			"labels": labels,
		}, &rz)
		if err != nil {
			return 0, fmt.Errorf("could not update zone labels %s: %w", origin, err)
		}
	}
	updateCount := 0
	updateRecords, records, err := buildUpsertRecords(z, changeset)
	if err != nil {
		return 0, fmt.Errorf("unable to build zone update records: %w", err)
	}
	if updateRecords {
		r, err := c.postRequest(fmt.Sprintf(pathDnsUpsertRecords, c.creds.Stack, z.ID))
		if err != nil {
			return 0, fmt.Errorf("unable to update zone records %s: %w", origin, err)
		}
		err = c.addAuth(r)
		if err != nil {
			return 0, fmt.Errorf("unable to update zone records auth: %w", err)
		}
		var zri recordsUpsertResponse
		err = c.doWithBody(r, map[string]interface{}{
			"records": records,
		}, &zri)
		if err != nil {
			return 0, fmt.Errorf("could not update zone records %s: %w", origin, err)
		}
		updateCount = updateCount + len(zri.ZoneRecordIds)
	}
	deleteRecords, deletes := buildDeleteRecords(z, changeset)
	if deleteRecords {
		r, err := c.postRequest(fmt.Sprintf(pathDnsDeleteRecords, c.creds.Stack, z.ID))
		if err != nil {
			return 0, fmt.Errorf("unable to delete zone records %s: %w", origin, err)
		}
		err = c.addAuth(r)
		if err != nil {
			return 0, fmt.Errorf("unable to delete zone records auth: %w", err)
		}
		err = c.doWithBody(r, map[string]interface{}{
			"zoneRecordIds": deletes,
		}, nil)
		if err != nil {
			var e StatusCodeError
			if !errors.As(err, &e) && e.StatusCode() != 204 {
				return 0, fmt.Errorf("could not delete zone records %s: %w", origin, err)
			}
		}
		updateCount = updateCount + len(deletes)
	}
	return updateCount, nil
}

func parseZoneLabels(z *dns.Zone) ([]dns.ExtendedRecord, error) {
	ret := make([]dns.ExtendedRecord, 0)
	if l, ok := z.Labels[dns.KeyNoteSOA]; ok {
		l, err := base64.StdEncoding.DecodeString(l)
		if err != nil {
			return nil, fmt.Errorf("unable to un64 SOA: %w", err)
		}
		d := dns.NoteSOA{}
		err = json.Unmarshal(l, &d)
		if err != nil {
			return nil, fmt.Errorf("unable to unmarshall SOA: %w", err)
		}
		ret = append(ret, dns.ExtendedRecord{
			DomainName: dns.CanonicalizeName(z.Domain),
			TimeToLive: d.TimeToLive,
			Class:      gozone.RecordClass_IN,
			Type:       gozone.RecordType_SOA,
			Data:       d.Data,
			Comment:    d.Comment,
			ZoneId:     z.ID,
		})
	} else {
		ret = append(ret, dns.ExtendedRecord{
			DomainName: dns.CanonicalizeName(z.Domain),
			TimeToLive: 86400,
			Class:      gozone.RecordClass_IN,
			Type:       gozone.RecordType_SOA,
			Data: []string{
				dns.CanonicalizeName(z.NameServers[0]),
				dns.CanonicalizeName(fmt.Sprintf("hostmaster.%s", z.Domain)),
				"(",
				time.Now().Format("2006010215"),
				"3600",
				"1800",
				"604800",
				"300",
				")",
			},
			Comment: "SOA automatically created by SPDNS tool",
			ZoneId:  z.ID,
		})
	}
	if l, ok := z.Labels[dns.KeyNoteNameservers]; ok {
		l, err := base64.StdEncoding.DecodeString(l)
		if err != nil {
			return nil, fmt.Errorf("unable to un64 NS: %w", err)
		}
		d := dns.NoteNameServers{}
		err = json.Unmarshal(l, &d)
		if err != nil {
			return nil, fmt.Errorf("unable to unmarshall NS: %w", err)
		}
		for _, ns := range d.Nameservers {
			ret = append(ret, dns.ExtendedRecord{
				DomainName: dns.CanonicalizeName(z.Domain),
				TimeToLive: ns.TimeToLive,
				Class:      gozone.RecordClass_IN,
				Type:       gozone.RecordType_NS,
				Data:       ns.Data,
				Comment:    ns.Comment,
				ZoneId:     z.ID,
			})
		}
	} else {
		for _, ns := range z.NameServers {
			ret = append(ret, dns.ExtendedRecord{
				DomainName: dns.CanonicalizeName(z.Domain),
				TimeToLive: 3600,
				Class:      gozone.RecordClass_IN,
				Type:       gozone.RecordType_NS,
				Data: []string{
					dns.CanonicalizeName(ns),
				},
				Comment: "NS automatically exported by SPDNS tool",
				ZoneId:  z.ID,
			})
		}
	}
	return ret, nil
}

func buildZoneLabels(z *dns.Zone, changeset []dns.ChangeRecord) (bool, map[string]string, error) {
	update := false
	ret := make(map[string]string)
	nns := &dns.NoteNameServers{}
	for _, rr := range changeset {
		if rr.Item.Type == gozone.RecordType_SOA {
			if rr.Action == dns.Add || rr.Action == dns.Preserve {
				d, err := json.Marshal(dns.NoteSOA{
					TimeToLive: rr.Item.TimeToLive,
					Data:       rr.Item.Data,
					Comment:    rr.Item.Comment,
				})
				if err != nil {
					return false, nil, fmt.Errorf("unable to marshall SOA: %w", err)
				}
				ret[dns.KeyNoteSOA] = base64.StdEncoding.EncodeToString(d)

			}
			if rr.Action == dns.Add || rr.Action == dns.Delete {
				update = true
			}
		}
		if rr.Item.Type == gozone.RecordType_NS && dns.NormalizeName(rr.Item.DomainName) == dns.NormalizeName(z.Domain) {
			// We only process NS at the apex, since it's possible for NS to be for subdomains
			if rr.Action == dns.Add || rr.Action == dns.Preserve {
				nns.Nameservers = append(nns.Nameservers, dns.NoteNS{
					TimeToLive: rr.Item.TimeToLive,
					Data:       rr.Item.Data,
					Comment:    rr.Item.Comment,
				})
			}
			if rr.Action == dns.Add || rr.Action == dns.Delete {
				update = true
			}
		}
	}
	if len(nns.Nameservers) > 0 {
		d, err := json.Marshal(nns)
		if err != nil {
			return false, nil, fmt.Errorf("unable to marshall NS: %w", err)
		}
		ret[dns.KeyNoteNameservers] = base64.StdEncoding.EncodeToString(d)
	}
	return update, ret, nil
}

func buildUpsertRecords(z *dns.Zone, changeset []dns.ChangeRecord) (bool, []map[string]string, error) {
	ret := make([]map[string]string, 0)
	update := false
	for _, r := range changeset {
		// Skip SOA
		if r.Item.Type == gozone.RecordType_SOA {
			continue
		}
		// Skip NS at the apex
		if r.Item.Type == gozone.RecordType_NS && dns.NormalizeName(r.Item.DomainName) == dns.NormalizeName(z.Domain) {
			continue
		}
		// Skip any that don't require an upsert call
		if r.Action != dns.Add && r.Action != dns.Modify {
			continue
		}
		i := make(map[string]string)
		label := dns.NormalizeName(strings.TrimSuffix(dns.NormalizeName(r.Item.DomainName), dns.NormalizeName(z.Domain)))
		if label == "" {
			label = "@"
		}
		i["name"] = label
		i["type"] = r.Item.Type.String()
		i["ttl"] = fmt.Sprintf("%d", int(r.Item.TimeToLive))
		i["data"] = strings.Join(r.Item.Data, " ")
		i["weight"] = "1" // TODO: Alternate weight options
		if r.Item.Comment != "" {
			labels := make(map[string]string)
			labels[dns.KeyNoteComment] = r.Item.Comment
			d, err := json.Marshal(labels)
			if err != nil {
				return false, nil, fmt.Errorf("unable to marshall upsert %s: %w", r.Item.String(), err)
			}
			i["labels"] = base64.StdEncoding.EncodeToString(d)
		}
		if r.Action == dns.Modify {
			i["id"] = r.Item.RecordId
		}
		ret = append(ret, i)
		update = true
	}
	return update, ret, nil
}

func buildDeleteRecords(z *dns.Zone, changeset []dns.ChangeRecord) (bool, []string) {
	ret := make([]string, 0)
	update := false
	for _, r := range changeset {
		// Skip SOA
		if r.Item.Type == gozone.RecordType_SOA {
			continue
		}
		// Skip NS at the apex
		if r.Item.Type == gozone.RecordType_NS && dns.NormalizeName(r.Item.DomainName) == dns.NormalizeName(z.Domain) {
			continue
		}
		// If it's not a delete, skip it
		if r.Action != dns.Delete {
			continue
		}
		ret = append(ret, r.Item.RecordId)
		update = true
	}
	return update, ret
}
