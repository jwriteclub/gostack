/*
 * GoStack API Client
 *
 *  Copyright 2022 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package waf

import (
	"strconv"
	"time"
)

type Interval time.Duration

func (i Interval) String() string {
	switch i {
	case IntervalMinutely:
		return "MINUTELY"
	case IntervalHourly:
		return "HOURLY"
	case IntervalDaily:
		return "DAILY"
	}
	panic("invalid interval")
}

const (
	IntervalMinutely Interval = Interval(time.Minute)
	IntervalHourly   Interval = Interval(time.Hour)
	IntervalDaily    Interval = Interval(time.Hour * 24)
)

type TrafficItem struct {
	Traffic   TrafficEntry
	StartTime time.Time
	Interval  Interval
}

type TrafficEntry struct {
	Timestamp         string `json:"timestamp"`
	Api               string `json:"api"`
	Ajax              string `json:"ajax"`
	Static            string `json:"static"`
	CustomBlocked     string `json:"customBlocked"`
	PolicyBlocked     string `json:"policyBlocked"`
	DdosBlocked       string `json:"ddosBlocked"`
	Monitored         string `json:"monitored"`
	CustomWhitelisted string `json:"customWhitelisted"`
	PolicyWhitelisted string `json:"policyWhitelisted"`
	OriginError4xx    string `json:"originError4xx"`
	OriginError5xx    string `json:"originError5xx"`
	OriginTimeout     string `json:"originTimeout"`
	Uncategorized     string `json:"uncategorized"`
	PassedToOrigin    string `json:"passedToOrigin"`
	ResponseTime      string `json:"responseTime"`
	Total             string `json:"total"`
}

func (t TrafficEntry) Numeric() (ret NumericTrafficEntry, err error) {
	ret.Timestamp, err = time.Parse("2006-01-02T15:04:05Z", t.Timestamp)
	if err != nil {
		return
	}

	var toU = func(s string) (uint64, error) {
		return strconv.ParseUint(s, 10, 64)
	}

	ret.Api, err = toU(t.Api)
	if err != nil {
		return
	}
	ret.Ajax, err = toU(t.Ajax)
	if err != nil {
		return
	}
	ret.Static, err = toU(t.Static)
	if err != nil {
		return
	}
	ret.CustomBlocked, err = toU(t.CustomBlocked)
	if err != nil {
		return
	}
	ret.PolicyBlocked, err = toU(t.PolicyBlocked)
	if err != nil {
		return
	}
	ret.DdosBlocked, err = toU(t.DdosBlocked)
	if err != nil {
		return
	}
	ret.Monitored, err = toU(t.Monitored)
	if err != nil {
		return
	}
	ret.CustomWhitelisted, err = toU(t.CustomWhitelisted)
	if err != nil {
		return
	}
	ret.PolicyWhitelisted, err = toU(t.PolicyWhitelisted)
	if err != nil {
		return
	}
	ret.OriginError4xx, err = toU(t.OriginError4xx)
	if err != nil {
		return
	}
	ret.OriginError5xx, err = toU(t.OriginError5xx)
	if err != nil {
		return
	}
	ret.OriginTimeout, err = toU(t.OriginTimeout)
	if err != nil {
		return
	}
	ret.Uncategorized, err = toU(t.Uncategorized)
	if err != nil {
		return
	}
	ret.PassedToOrigin, err = toU(t.PassedToOrigin)
	if err != nil {
		return
	}

	ret.ResponseTime, err = strconv.ParseFloat(t.ResponseTime, 64)
	if err != nil {
		return
	}

	ret.Total, err = toU(t.Total)
	if err != nil {
		return
	}

	return
}

type NumericTrafficEntry struct {
	Timestamp         time.Time
	Api               uint64
	Ajax              uint64
	Static            uint64
	CustomBlocked     uint64
	PolicyBlocked     uint64
	DdosBlocked       uint64
	Monitored         uint64
	CustomWhitelisted uint64
	PolicyWhitelisted uint64
	OriginError4xx    uint64
	OriginError5xx    uint64
	OriginTimeout     uint64
	Uncategorized     uint64
	PassedToOrigin    uint64
	ResponseTime      float64
	Total             uint64
}

func (n NumericTrafficEntry) IsEmpty() bool {
	if n.Api > 0 {
		return false
	}
	if n.Ajax > 0 {
		return false
	}
	if n.Static > 0 {
		return false
	}
	if n.CustomBlocked > 0 {
		return false
	}
	if n.PolicyBlocked > 0 {
		return false
	}
	if n.DdosBlocked > 0 {
		return false
	}
	if n.Monitored > 0 {
		return false
	}
	if n.CustomWhitelisted > 0 {
		return false
	}
	if n.PolicyWhitelisted > 0 {
		return false
	}
	if n.OriginError4xx > 0 {
		return false
	}
	if n.OriginError5xx > 0 {
		return false
	}
	if n.OriginTimeout > 0 {
		return false
	}
	if n.Uncategorized > 0 {
		return false
	}
	if n.PassedToOrigin > 0 {
		return false
	}
	if n.ResponseTime > 0 {
		return false
	}
	if n.Total > 0 {
		return false
	}
	return true
}
