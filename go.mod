module bitbucket.org/jwriteclub/gostack

go 1.16

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d
	github.com/pelletier/go-toml/v2 v2.0.7 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/spf13/afero v1.9.5 // indirect
	github.com/spf13/viper v1.15.0
	github.com/tenta-browser/polychromatic v0.0.2
	github.com/wpalmer/gozone v0.0.0-20190618201257-3b1011627457
	golang.org/x/crypto v0.9.0 // indirect
)
