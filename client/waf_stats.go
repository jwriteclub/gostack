/*
 * GoStack API Client
 *
 *  Copyright 2022 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gostack/models/common"
	"bitbucket.org/jwriteclub/gostack/models/waf"
	"fmt"
	"time"
)

const (
	pathWafTraffic = "waf/v2/stacks/%s/traffic"
)

func (c *Client) ListWafTraffic(stackId, siteId string, interval waf.Interval, start, end time.Time) ([]waf.TrafficItem, error) {
	r, err := c.getRequest(fmt.Sprintf(pathWafTraffic, stackId))
	if err != nil {
		return nil, fmt.Errorf("unable to list waf traffic request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list waf traffic auth: %w", err)
	}
	var s wafTrafficAPIResponse
	err = c.doWithArgs(r, map[string]string{
		"start_date": start.Format(common.StackPathDateFormat),
		"end_date":   end.Format(common.StackPathDateFormat),
		"site_id":    siteId,
		"resolution": interval.String(),
	}, &s)
	if err != nil {
		return nil, fmt.Errorf("could not list waf traffic: %w", err)
	}
	ret := make([]waf.TrafficItem, 0, len(s.Traffic))
	for _, t := range s.Traffic {
		s, err := time.Parse(common.StackPathDateFormat, t.Timestamp)
		if err != nil {
			return nil, err
		}
		ret = append(ret, waf.TrafficItem{
			Traffic:   t,
			StartTime: s,
			Interval:  interval,
		})
	}
	return ret, nil
}
