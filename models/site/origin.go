/*
 * GoStack API Client
 *
 *  Copyright 2023 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package site

type Origin struct {
	Id         string     `json:"id"`
	StackId    string     `json:"stackId"`
	SiteId     string     `json:"siteId"`
	Dedicated  bool       `json:"dedicated"`
	HTTPOrigin HTTPOrigin `json:"http"`
}

type HTTPOrigin struct {
	Path                  string               `json:"path"`
	Host                  string               `json:"hostname"`
	Port                  int                  `json:"port"`
	SecurePort            int                  `json:"securePort"`
	Authentication        OriginAuthentication `json:"authentication"`
	VerifyCertificate     bool                 `json:"verifyCertificate"`
	CertificateCommonName string               `json:"certificateCommonName"`
}

type OriginAuthentication struct {
	Basic OriginBasicAuthentication `json:"basic"`
	S3    OriginS3Authentication    `json:"s3"`
}

type OriginBasicAuthentication struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type OriginS3Authentication struct {
	Key    string `json:"accessKey"`
	Secret string `json:"secretKey"`
}
