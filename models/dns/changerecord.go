/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package dns

import (
	"fmt"
	"github.com/mgutz/ansi"
)

type ChangeType int

func (c ChangeType) String() string {
	switch c {
	case Add:
		return "Add"
	case Preserve:
		return "Preserve"
	case Modify:
		return "Modify"
	case Delete:
		return "Delete"
	default:
		panic(fmt.Sprintf("unknown change type %d", int(c)))
	}
}

func (c ChangeType) ActionString() string {
	switch c {
	case Add:
		return "+"
	case Preserve:
		return " "
	case Modify:
		return "*"
	case Delete:
		return "-"
	default:
		panic(fmt.Sprintf("unknown change type %d", int(c)))
	}
}

const (
	Add = iota
	Preserve
	Modify // TODO: Try and use modify when possible
	Delete
)

type ChangeRecord struct {
	Action ChangeType
	Item   ExtendedRecord
}

func (c ChangeRecord) String() string {
	return c.Action.ActionString() + " " + c.Item.String()
}

func (c ChangeRecord) ColorString() string {
	switch c.Action {
	case Add:
		return ansi.Color(c.Action.ActionString(), ansi.Green) + " " + ansi.Color(c.Item.String(), ansi.LightGreen)
	case Preserve:
		return " " + " " + c.Item.String()
	case Modify:
		return ansi.Color(c.Action.ActionString(), ansi.Magenta) + " " + ansi.Color(c.Item.String(), ansi.LightMagenta)
	case Delete:
		return ansi.Color(c.Action.ActionString(), ansi.Red) + " " + ansi.Color(c.Item.String(), ansi.LightRed)
	default:
		panic(fmt.Sprintf("unknown change type %d", int(c.Action)))
	}
}
