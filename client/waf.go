/*
 * GoStack API Client
 *
 *  Copyright 2023 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gostack/models/site"
	"fmt"
)

const (
	pathStackWafRules       = "waf/v1/stacks/%s/sites/%s/rules"
	pathStackWafRulesDelete = "waf/v1/stacks/%s/sites/%s/rules/%s"
)

func (c *Client) ListWafRules(stackId, siteId string) ([]site.Rule, error) {
	r, err := c.getRequest(fmt.Sprintf(pathStackWafRules, stackId, siteId))
	if err != nil {
		return nil, fmt.Errorf("unable to list site rules request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list site rules auth: %w", err)
	}
	rules := make([]site.Rule, 0)
	after := "0"
	for {
		var s listSitesRulesAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
		}, &s)
		if err != nil {
			return nil, fmt.Errorf("could not site rules: %w", err)
		}
		rules = append(rules, s.Rules...)
		if s.PI.HasNextPage {
			after = s.PI.EndCursor
		} else {
			break
		}
	}
	return rules, nil
}

func (c *Client) AddWafRule(stackId, siteId string, body map[string]interface{}) error {
	r, err := c.postRequest(fmt.Sprintf(pathStackWafRules, stackId, siteId))
	if err != nil {
		return fmt.Errorf("unable to add site rule request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return fmt.Errorf("unable to add site rule auth: %w", err)
	}
	var s addSiteRuleAPIResponse
	err = c.doWithBody(r, body, &s)
	if err != nil {
		return fmt.Errorf("could not add site rule: %w", err)
	}
	return nil
}

func (c *Client) DeleteWafRule(stackId, siteId, ruleId string) error {
	r, err := c.deleteRequest(fmt.Sprintf(pathStackWafRulesDelete, stackId, siteId, ruleId))
	if err != nil {
		return fmt.Errorf("unable to delete site rule request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return fmt.Errorf("unable to delete site rule auth: %w", err)
	}
	err = c.doBareRequest(r)
	if err != nil {
		return fmt.Errorf("could not delete site rule: %w", err)
	}
	return nil
}
