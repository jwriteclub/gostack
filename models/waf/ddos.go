/*
 * GoStack API Client
 *
 *  Copyright 2022 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package waf

import "strconv"

type DDoS struct {
	Global    string `json:"globalThreshold"`
	Burst     string `json:"burstThreshold"`
	SubSecond string `json:"subSecondBurstThreshold"`
}

func (d DDoS) GetGlobal() (int64, error) {
	return strconv.ParseInt(d.Global, 10, 64)
}
func (d DDoS) MustGetGlobal() int {
	ret, err := d.GetGlobal()
	if err != nil {
		panic(err.Error())
	}
	return int(ret)
}

func (d DDoS) GetBurst() (int64, error) {
	return strconv.ParseInt(d.Burst, 10, 64)
}
func (d DDoS) MustGetBurst() int {
	ret, err := d.GetBurst()
	if err != nil {
		panic(err.Error())
	}
	return int(ret)
}

func (d DDoS) GetSubSecond() (int64, error) {
	return strconv.ParseInt(d.SubSecond, 10, 64)
}
func (d DDoS) MustGetSubSecond() int {
	ret, err := d.GetSubSecond()
	if err != nil {
		panic(err.Error())
	}
	return int(ret)
}
