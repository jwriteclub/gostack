/*
 * GoStack API Client
 *
 *  Copyright 2023 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package site

import (
	"fmt"
	"strings"
)

type MaybeEmpty interface {
	IsEmpty() bool
}

type IPCondition struct {
	IP string `json:"ipAddress"`
}

func (i IPCondition) IsEmpty() bool {
	return i.IP == ""
}

func (i IPCondition) String() string {
	ipl := strings.Split(i.IP, ",")
	if len(ipl) > 1 {
		return fmt.Sprintf("IP is one of (%s)", strings.Join(ipl, ", "))
	}
	return fmt.Sprintf("IP is %s", i.IP)
}

type IPRangeCondition struct {
	Begin string `json:"lowerBound"`
	End   string `json:"upperBound"`
}

func (i IPRangeCondition) IsEmpty() bool {
	return i.Begin == "" && i.End == ""
}

func (i IPRangeCondition) String() string {
	return fmt.Sprintf("IP is between %s and %s", i.Begin, i.End)
}

type URLCondition struct {
	URL        string `json:"url"`
	ExactMatch bool   `json:"exactMatch"`
}

func (u URLCondition) IsEmpty() bool {
	return u.URL == "" && !u.ExactMatch
}

func (u URLCondition) String() string {
	match := "LIKE"
	if u.ExactMatch {
		match = "EXACTLY"
	}
	return fmt.Sprintf("URL is %s %s", match, u.URL)
}

type UserAgentCondition struct {
	UserAgent  string `json:"userAgent"`
	ExactMatch bool   `json:"exactMatch"`
}

func (u UserAgentCondition) IsEmpty() bool {
	return u.UserAgent == "" && !u.ExactMatch
}

func (u UserAgentCondition) String() string {
	match := "LIKE"
	if u.ExactMatch {
		match = "EXACTLY"
	}
	return fmt.Sprintf("UserAgent is %s %s", match, u.UserAgent)
}

type HeaderLiteralCondition struct {
	Header string `json:"header"`
}

func (h HeaderLiteralCondition) IsEmpty() bool {
	return h.Header == ""
}

func (h HeaderLiteralCondition) String() string {
	return fmt.Sprintf("HEADER %s exists", h.Header)
}

type HeaderValueCondition struct {
	Header     string `json:"header"`
	Value      string `json:"value"`
	ExactMatch bool   `json:"exactMatch"`
}

func (h HeaderValueCondition) IsEmpty() bool {
	return h.Header == "" && h.Value == "" && !h.ExactMatch
}

func (h HeaderValueCondition) String() string {
	match := "LIKE"
	if h.ExactMatch {
		match = "EXACTLY"
	}
	return fmt.Sprintf("HEADER %s is %s %s", h.Header, match, h.Value)
}

type HTTPMethodCondition struct {
	Method string `json:"httpMethod"`
}

func (h HTTPMethodCondition) IsEmpty() bool {
	return h.Method == ""
}

func (h HTTPMethodCondition) String() string {
	return fmt.Sprintf("HTTP METHOD is %s", h.Method)
}

type FileExtensionCondition struct {
	Extension string `json:"fileExtension"`
}

func (f FileExtensionCondition) IsEmpty() bool {
	return f.Extension == ""
}

func (f FileExtensionCondition) String() string {
	return fmt.Sprintf("FILE EXTENSION is %s", f.Extension)
}

type ContentTypeCondition struct {
	Type string `json:"contentType"`
}

func (c ContentTypeCondition) IsEmpty() bool {
	return c.Type == ""
}

func (c ContentTypeCondition) String() string {
	return fmt.Sprintf("CONTENT TYPE is %s", c.Type)
}

type CountryCondition struct {
	Code string `json:"countryCode"`
}

func (c CountryCondition) IsEmpty() bool {
	return c.Code == ""
}

func (c CountryCondition) String() string {
	return fmt.Sprintf("COUNTRY is %s", c.Code)
}

type OrganizationCondition struct {
	Organization string `json:"organization"`
}

func (o OrganizationCondition) IsEmpty() bool {
	return o.Organization == ""
}

func (o OrganizationCondition) String() string {
	return fmt.Sprintf("ORGANIZATION is %s", o.Organization)
}

type RequestRateCondition struct {
	Ips         []string `json:"ips"`
	HTTPMethods []string `json:"httpMethods"`
	PathPattern string   `json:"pathPattern"`
	Requests    string   `json:"requests"`
	Time        string   `json:"time"`
}

func (r RequestRateCondition) IsEmpty() bool {
	return len(r.Ips) == 0 && len(r.HTTPMethods) == 0 && r.PathPattern == "" && r.Requests == "" && r.Time == ""
}

func (r RequestRateCondition) String() string {
	path := ""
	if r.PathPattern != "" {
		path = fmt.Sprintf(" for path %s", r.PathPattern)
	}
	ips := ""
	if r.Ips != nil && len(r.Ips) > 0 {
		ips = fmt.Sprintf(" from ips in (%s)", strings.Join(r.Ips, ", "))
	}
	methods := ""
	if r.HTTPMethods != nil && len(r.HTTPMethods) > 0 {
		methods = fmt.Sprintf(" with methods (%s)", strings.Join(r.HTTPMethods, ", "))
	}
	return fmt.Sprintf("REQUEST RATE is %s requests per %s seconds%s%s%s", r.Requests, r.Time, ips, methods, path)
}

type OwnerTypeCondition struct {
	Types []string `json:"ownerTypes"`
}

func (o OwnerTypeCondition) IsEmpty() bool {
	return len(o.Types) == 0
}

func (o OwnerTypeCondition) String() string {
	return fmt.Sprintf("OWNER TYPE is in (%s)", strings.Join(o.Types, ", "))
}

type TagCondition struct {
	Tags []string `json:"tags"`
}

func (t TagCondition) IsEmpty() bool {
	return len(t.Tags) == 0
}

func (t TagCondition) String() string {
	return fmt.Sprintf("TAG is in (%s)", strings.Join(t.Tags, ", "))
}

type SessionRequestCondition struct {
	Count string `json:"requestCount"`
}

func (s SessionRequestCondition) IsEmpty() bool {
	return s.Count == ""
}

func (s SessionRequestCondition) String() string {
	return fmt.Sprintf("SESSION REQUEST COUNT is %s", s.Count)
}

type Condition struct {
	IP                   IPCondition             `json:"ip"`
	IPRange              IPRangeCondition        `json:"ipRange"`
	URL                  URLCondition            `json:"url"`
	UserAgent            UserAgentCondition      `json:"userAgent"`
	Header               HeaderValueCondition    `json:"header"`
	HeaderExists         HeaderLiteralCondition  `json:"headerExists"`
	ResponseHeader       HeaderValueCondition    `json:"responseHeader"`
	ResponseHeaderExists HeaderLiteralCondition  `json:"responseHeaderExists"`
	HTTPMethod           HTTPMethodCondition     `json:"httpMethod"`
	FileExtension        FileExtensionCondition  `json:"fileExtension"`
	ContentType          ContentTypeCondition    `json:"contentType"`
	Country              CountryCondition        `json:"country"`
	Organization         OrganizationCondition   `json:"organization"`
	RequestRate          RequestRateCondition    `json:"requestRate"`
	OwnerTypes           OwnerTypeCondition      `json:"ownerTypes"`
	Tags                 TagCondition            `json:"tags"`
	RequestCount         SessionRequestCondition `json:"sessionRequestCount"`
	Negation             bool                    `json:"negation"`
}

func (c Condition) IsIPOnly() bool {
	return (!c.IP.IsEmpty() || !c.IPRange.IsEmpty()) &&
		c.URL.IsEmpty() && c.UserAgent.IsEmpty() && c.Header.IsEmpty() && c.HeaderExists.IsEmpty() &&
		c.ResponseHeader.IsEmpty() && c.ResponseHeaderExists.IsEmpty() && c.HTTPMethod.IsEmpty() &&
		c.FileExtension.IsEmpty() && c.ContentType.IsEmpty() && c.Country.IsEmpty() &&
		c.Organization.IsEmpty() && c.RequestRate.IsEmpty() && c.OwnerTypes.IsEmpty() &&
		c.Tags.IsEmpty() && c.RequestCount.IsEmpty()
}

func (c Condition) String() string {
	var conditions []string
	if !c.IP.IsEmpty() {
		conditions = append(conditions, c.IP.String())
	}
	if !c.IPRange.IsEmpty() {
		conditions = append(conditions, c.IPRange.String())
	}
	if !c.URL.IsEmpty() {
		conditions = append(conditions, c.URL.String())
	}
	if !c.UserAgent.IsEmpty() {
		conditions = append(conditions, c.UserAgent.String())
	}
	if !c.Header.IsEmpty() {
		conditions = append(conditions, c.Header.String())
	}
	if !c.HeaderExists.IsEmpty() {
		conditions = append(conditions, c.HeaderExists.String())
	}
	if !c.ResponseHeader.IsEmpty() {
		conditions = append(conditions, c.ResponseHeader.String())
	}
	if !c.ResponseHeaderExists.IsEmpty() {
		conditions = append(conditions, c.ResponseHeaderExists.String())
	}
	if !c.HTTPMethod.IsEmpty() {
		conditions = append(conditions, c.HTTPMethod.String())
	}
	if !c.FileExtension.IsEmpty() {
		conditions = append(conditions, c.FileExtension.String())
	}
	if !c.ContentType.IsEmpty() {
		conditions = append(conditions, c.ContentType.String())
	}
	if !c.Country.IsEmpty() {
		conditions = append(conditions, c.Country.String())
	}
	if !c.Organization.IsEmpty() {
		conditions = append(conditions, c.Organization.String())
	}
	if !c.RequestRate.IsEmpty() {
		conditions = append(conditions, c.RequestRate.String())
	}
	if !c.RequestCount.IsEmpty() {
		conditions = append(conditions, c.RequestCount.String())
	}
	if !c.OwnerTypes.IsEmpty() {
		conditions = append(conditions, c.OwnerTypes.String())
	}
	if !c.Tags.IsEmpty() {
		conditions = append(conditions, c.Tags.String())
	}
	out := strings.Join(conditions, " AND ")
	if c.IsIPOnly() {
		out = strings.Join(conditions, " OR ")
	}
	if c.Negation {
		out = fmt.Sprintf("NOT (%s)", out)
	}
	return out
}
