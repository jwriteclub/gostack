/*
 * GoStack API Client
 *
 *  Copyright 2021 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gostack/models/common"
	"bitbucket.org/jwriteclub/gostack/models/waf"
	"fmt"
	"time"
)

const (
	pathWafRequestStats   = "waf/v1/stacks/%s/sites/%s/request_stats"
	pathWafRequestsAll    = "waf/v1/stacks/%s/sites/%s/requests"
	pathWafRequestInfo    = "waf/v1/stacks/%s/sites/%s/requests/%s"
	pathWafRequestDetails = "waf/v1/stacks/%s/sites/%s/requests/%s/details"
)

func (c *Client) WafRequestStats(stackId, siteId string, start, end time.Time) (*waf.RequestStatistics, error) {
	r, err := c.getRequest(fmt.Sprintf(pathWafRequestStats, stackId, siteId))
	if err != nil {
		return nil, fmt.Errorf("unable to load waf statistics request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to load waf statistics auth: %w", err)
	}
	var ret wafRequestStatisticsAPIResponse
	err = c.doWithArgs(r, map[string]string{
		"start_date":          start.Format(common.StackPathDateFormat),
		"end_date":            end.Format(common.StackPathDateFormat),
		"filter.action_value": "ANY_ACTION",
		"filter.result_value": "ANY_RESULT",
	}, &ret)
	if err != nil {
		return nil, fmt.Errorf("could not load waf statistics: %w", err)
	}
	return &ret.Stats, nil
}

func (c *Client) ListWafRequests(stackId, siteId, trafficType string, start, end time.Time) ([]waf.RequestDetails, error) {
	r, err := c.getRequest(fmt.Sprintf(pathWafRequestsAll, stackId, siteId))
	if err != nil {
		return nil, fmt.Errorf("unable to list waf requests request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to list waf requests auth: %w", err)
	}
	requests := make([]waf.RequestDetails, 0)
	after := "0"
	for {
		var s listWafRequestsAPIResponse
		err = c.doWithArgs(r, map[string]string{
			"page_request.first": perPage,
			"page_request.after": after,
			"start_date":         start.Format(common.StackPathDateFormat),
			"end_date":           end.Format(common.StackPathDateFormat),
			"traffic_type":       trafficType,
		}, &s)
		if err != nil {
			return nil, fmt.Errorf("could not list waf requests: %w", err)
		}
		requests = append(requests, s.Requests...)
		if s.PI.HasNextPage {
			after = s.PI.EndCursor
		} else {
			break
		}
	}
	return requests, nil
}
