/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package site

type Feature string

const (
	FeatureCDN       Feature = "CDN"
	FeatureWAF               = "WAF"
	FeatureDNS               = "DNS"
	FeatureScripting         = "Scripting"
	FeatureUnknown           = "UNKNOWN"
)

type Site struct {
	Id         string   `json:"id"`
	Label      string   `json:"label"`
	Features   []string `json:"features"`
	ApiUrls    []string `json:"apiUrls"`
	Monitoring bool     `json:"monitoring"`
	Created    string   `json:"createdAt"`
	Updated    string   `json:"updatedAt"`
	Status     string   `json:"status"`
}

func (s Site) HasFeature(feat Feature) bool {
	for _, f := range s.Features {
		if f == string(feat) {
			return true
		}
	}
	return false
}

func (s Site) GetFeatures() []Feature {
	ret := make([]Feature, 0)
	for _, f := range s.Features {
		for _, t := range []Feature{FeatureCDN, FeatureWAF, FeatureDNS, FeatureScripting} {
			if f == string(t) {
				ret = append(ret, t)
				break
			}
		}
		ret = append(ret, FeatureUnknown)
	}
	return ret
}
