/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/jwriteclub/gostack/config"
	"bitbucket.org/jwriteclub/gostack/models/auth"
)

const (
	pathAuthenticate = "identity/v1/oauth2/token"
)

const perPage = "100" // This appears to be an undocumented StackPath hardcoded maximum

var (
	ErrDomainNotFound = errors.New("domain not found")
)

type Client struct {
	endp  string
	creds *config.Credentials
	h     *http.Client
	t     *auth.Token
}

func DefaultClient() (*Client, error) {
	creds, err := config.GetCredentials()
	if err != nil {
		return nil, fmt.Errorf("unable to create default config: %w", err)
	}
	return NewClient(config.DefaultEndpoint, creds)
}

func NewClient(endpoint string, credentials *config.Credentials) (*Client, error) {
	ret := &Client{creds: credentials, endp: endpoint, h: &http.Client{
		Transport: &http.Transport{
			ForceAttemptHTTP2:   true,
			MaxIdleConns:        10,
			MaxIdleConnsPerHost: 10,
			IdleConnTimeout:     60 * time.Second,
		},
		Timeout: 60 * time.Second,
	}}
	err := ret.Authenticate()
	if err != nil {
		return nil, fmt.Errorf("unable to create new client: %w", err)
	}
	return ret, nil
}

func (c *Client) DefaultStack() string {
	return c.creds.Stack
}

func (c *Client) Authenticate() error {
	r, err := c.postRequest(pathAuthenticate)
	if err != nil {
		return fmt.Errorf("unable to authenticate: %w", err)
	}
	var t auth.Token
	err = c.doWithBody(r, map[string]interface{}{
		"grant_type":    "client_credentials",
		"client_id":     c.creds.ClientId,
		"client_secret": c.creds.ClientSecret,
	}, &t)
	if err != nil {
		return fmt.Errorf("could not authenticate: %w", err)
	}
	t.ExpiresAt = time.Now().Add(time.Duration(t.ExpiresIn * int(time.Second)))
	c.t = &t
	return nil
}
